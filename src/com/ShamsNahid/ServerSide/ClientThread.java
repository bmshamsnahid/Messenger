package com.ShamsNahid.ServerSide;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import com.ShamsNahid.Helper.ChatMessage;


/** One instance of this thread will run for each client */
class ClientThread extends Thread {
		Socket socket;	//client socket to transmit data
		ObjectInputStream sInput;	
		ObjectOutputStream sOutput;
		int id;	//client unique id will be assigned here
		String username;	//client name
		ChatMessage cm;	//client message
		private boolean clientThreadStatus;
		ArrayList<ClientThread> alClientThread;
		
		ClientThread(Socket socket, int uniqueId, ArrayList<ClientThread> alClientThread) {	//server send the socket
			id = ++uniqueId;
			this.socket = socket;
			this.alClientThread = alClientThread;
			
			try {
				sOutput = new ObjectOutputStream(socket.getOutputStream());
				sInput  = new ObjectInputStream(socket.getInputStream());
				
				username = (String) sInput.readObject();
				System.out.println(username + " found.");
			} catch (Exception e) {
				System.out.println("Exception on creating stream in cleitn thread:  " + e.toString());
				return;
			}
			
		}

		public void run() {
			clientThreadStatus = true;
			
			while(clientThreadStatus) {
				
				try {
					cm = (ChatMessage) sInput.readObject();
				}
				catch (Exception e) {
					System.out.println(username + " Exception reading Streams: " + e);
					break;				
				}
				
				String message = cm.getMessage();
				
				int type = cm.getType();
				
				if(type == 0) {
					String onlineStatus = "";
					for(ClientThread clientThread : alClientThread) {
						onlineStatus += clientThread.username + " ";
					}
					System.out.println("Type Test: requesting online list");
					//writeMsg(onlineStatus + "\n");
					try {this.sOutput.writeObject(onlineStatus);} catch(Exception e) { }
				}
				else if(type == 1) {
					System.out.println("Type Test: sending message");
					for(ClientThread clientThread : alClientThread) {
						try {clientThread.sOutput.writeObject(message + "\n");} catch(Exception e) { }
					}
				} else if(type == 2) {
					System.out.println("Type Test: logging out");
					clientThreadStatus = false;
					for(int index=0; index<alClientThread.size(); index++) {
						ClientThread ct = alClientThread.get(index);
						if(ct.username.equals(username)) {
							alClientThread.remove(ct);
							break;
						}
					}
				}
				
			}
			
			try {
				sOutput.close();
				sInput.close();
				socket.close();
			} catch(Exception e) {
				System.out.println("Exception in clsoing client Threead: " + e.toString());
			}
		}
		
	}