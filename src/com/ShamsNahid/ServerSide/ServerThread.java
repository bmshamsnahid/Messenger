package com.ShamsNahid.ServerSide;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * This is server
 * ->
*/
public class ServerThread {
	private static int uniqueId;	//each client will have id and start from 0
	private ArrayList<ClientThread> alClientThread;		//all coneeceted client will be stored here 
	
	private int port;	//server port, here client will be connected
	
	private boolean servetStatus;	//server will be running till keepGoing is true
	private boolean clientThreadStatus;

	public ServerThread(int port) {
		this.port = port;	//assign port
		alClientThread = new ArrayList<ClientThread>(); 	//declareing the arrayList
	}
	
	public void start() {
		servetStatus = true;	//true means thread is running i.e server is running
		try {
			ServerSocket serverSocket = new ServerSocket(port);		//creaqting server socket and wait for client to connect
			while(servetStatus) { //if a client is found then the client must be added 
				System.out.println("Server is on " + port);				
				Socket socket = serverSocket.accept();	//client is connected
				if(!servetStatus) break;	//if need to stop server
				
				ClientThread clientThread = new ClientThread(socket, uniqueId, alClientThread);  // make a thread of it
				alClientThread.add(clientThread);									// save it in the ArrayList
				clientThread.start();
			}
			
			try {
				serverSocket.close();	//closing server Socket
				
				for(int i = 0; i < alClientThread.size(); ++i) {	//trversing all client
					ClientThread tc = alClientThread.get(i);	//get each clientThread
					tc.sInput.close();		//closing input stream
					tc.sOutput.close();		//closing outputStream
					tc.socket.close();		//closing socket
				}
			}catch(Exception e) {
				
			}
		} catch (Exception e) {
            System.out.println("Exception on creating server and connecting client: " + e.toString());
		}
	}	

	
}

