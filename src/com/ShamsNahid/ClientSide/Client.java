package com.ShamsNahid.ClientSide;
import java.net.*;
import java.io.*;
import java.util.*;

import com.ShamsNahid.Helper.ChatMessage;

/*
 * The Client that can be run both as a console or a GUI
 */
public class Client  {

	private ObjectInputStream sInput;		// to read from the socket
	private ObjectOutputStream sOutput;		// to write on the socket
	private Socket socket;

	// if I use a GUI or not
	private ClientGUI cg;
	
	// the server, the port and the username
	private String server, username;
	private int port;
	
	/*
	 * Constructor call when used from a GUI
	 * in console mode the ClienGUI parameter is null
	 */
	Client(String server, int port, String username, ClientGUI cg) {
		this.server = server;
		this.port = port;
		this.username = username;
		// save if we are in GUI mode or not
		this.cg = cg;
	}
	
	/*
	 * To start the dialog
	 */
	public boolean start() {
		// try to connect to the server
		try {
			socket = new Socket(server, port);
		} 
		// if it failed not much I can so
		catch(Exception ec) {
			System.out.println("Error connectiong to server:" + ec);
			return false;
		}
		
		String msg = "Connection accepted " + socket.getInetAddress() + ":" + socket.getPort();
		System.out.println(msg);
	
		/* Creating both Data Stream */
		try
		{
			sInput  = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		}
		catch (IOException eIO) {
			System.out.println("Exception creating new Input/output Streams: " + eIO);
			return false;
		}

		// creates the Thread to listen from the server 
 		new ListenFromServer().start();
		// Send our username to the server this is the only message that we
		// will send as a String. All other messages will be ChatMessage objects
		try
		{
			sOutput.writeObject(username);
		}
		catch (IOException eIO) {
			System.out.println("Exception doing login : " + eIO);
			//so closing all connection
			try {sInput.close(); } catch(Exception e) {}
			try {sOutput.close(); } catch(Exception e) {}
			try {socket.close(); } catch(Exception e) {}
			return false;
		}
		// success we inform the caller that it worked
		return true;
	}

	/*
	 * To send a message to the console or the GUI
	 
	private void display(String msg) {
		cg.append(msg + "\n");		// append to the ClientGUI JTextArea (or whatever)
	}*/
	
	/*
	 * To send a message to the server
	 */
	void sendMessage(ChatMessage msg) {
		try { 
			sOutput.writeObject(msg); 
		}
		catch(Exception e) {
			System.out.println("Exception writing to server: " + e);
		}
	}	

	/*
	 * a class that waits for the message from the server and append them to the JTextArea
	 * if we have a GUI or simply System.out.println() it in console mode
	 */
	class ListenFromServer extends Thread {

		public void run() {
			while(true) {
				try {
					String msg = (String) sInput.readObject();
					cg.append(msg);
				}
				catch(Exception e) {
					
				}
			}
		}
	}
}
