package com.ShamsNahid.ClientSide;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.collections.*;
import javafx.event.*;
import java.util.*;

import com.ShamsNahid.Helper.ChatMessage;

public class ClientGUI extends Application {

	private Label labelAddress, labelPort;
	private TextField textFieldNameMessage;
	private TextField textFieldServerAddress, textFieldPortNumber;
	private Button buttonLogin, buttonLogOut, buttonWhoIsIn, buttonSend;
	private TextArea textAreaMessageField;
	private ListView listViewWhoIsOnline;
	
	private boolean connected;
	private Client client;
	
	private HBox hbox1, hbox2, hbox3;
	private VBox vbox1, vbox2;
	private BorderPane bp;
	private Scene scene;
	private Stage window;
	
	private String userName, serverAddress;
	private int portNumber;
	
	public void start(Stage window) throws Exception {
		this.window = window;
		try {
			initialize();
		} catch(Exception e) {
			System.out.println("Exception: " + e.toString());
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private void initialize() {
		text_field_initialize();
		text_area_initialize();
		lable_initialize();
		listViewInitialize();
		button_initialize();
		set_layout();
		show_layout();
	}
	
	private void listViewInitialize() {
		listViewWhoIsOnline = new ListView<>();
		listViewWhoIsOnline.getItems().addAll("a", "b", "c", "d");
		listViewWhoIsOnline.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	}

	private void show_layout() {
		scene = new Scene(bp, 500, 500);
		window.setScene(scene);
		window.show();
	}

	private void lable_initialize() {
		labelAddress = new Label("Enter Address");
		labelPort = new Label("Enter port");
	}

	private void set_layout() {
		bp = new BorderPane();
		
		GridPane componentHolderTop = new GridPane();
		componentHolderTop.add(labelAddress, 0, 0);
		componentHolderTop.add(textFieldServerAddress, 1, 0);
		componentHolderTop.add(labelPort, 2, 0);
		componentHolderTop.add(textFieldPortNumber, 3, 0);
		componentHolderTop.add(textFieldNameMessage, 1, 1);
		componentHolderTop.add(buttonSend, 2, 1);
		
		hbox3 = new HBox();
		hbox3.getChildren().addAll(buttonLogin, buttonLogOut, buttonWhoIsIn);
		
		bp.setTop(componentHolderTop);
		bp.setLeft(listViewWhoIsOnline);
		bp.setCenter(textAreaMessageField);
		bp.setBottom(hbox3);

	}

	private void button_initialize() {
		buttonLogin = new Button("LogIn");
		buttonLogOut = new Button("LogOut");
		buttonWhoIsIn = new Button("Who is In");
		buttonSend = new Button("Send");
		
		buttonSend.setOnAction(e -> {
			client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, textFieldNameMessage.getText().toString()));
			textFieldNameMessage.setText("");
		});
		
		buttonLogin.setOnAction(e -> {
			//button to login
			loginToServer();
		});
		
		buttonLogOut.setOnAction(e -> {
			client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, ""));
		});
		
		buttonWhoIsIn.setOnAction(e -> {
			client.sendMessage(new ChatMessage(ChatMessage.WHOISIN, ""));				
		});
	}

	private void loginToServer() {
		try {
			serverAddress = textFieldServerAddress.getText().toString().trim();
			userName = textFieldNameMessage.getText().toString().trim();
			portNumber = Integer.parseInt(textFieldPortNumber.getText().toString().trim());
			client = new Client(serverAddress, portNumber, userName, this);
		} catch(Exception e) {
			System.out.println("On login: Exception : " + e.toString());
			return;
		}
		textFieldNameMessage.setText("");
		textFieldNameMessage.setPromptText("Message");
		window.setTitle(userName);
		client.start();
		connected = true;
		hbox1.setVisible(false);
	}

	private void text_area_initialize() {
		textAreaMessageField = new TextArea();
		textAreaMessageField.setScrollLeft(0);
		textAreaMessageField.setScrollTop(0);
	}

	private void text_field_initialize() {
		textFieldServerAddress = new TextField("localhost");
		textFieldServerAddress.setPromptText("Server Address");
		
		textFieldPortNumber = new TextField("1100");
		textFieldPortNumber.setPromptText("Port");
		
		textFieldNameMessage = new TextField("Name");
		textFieldNameMessage.setPromptText("Name");
	}

	void append(String str) {
		textAreaMessageField.setText(textAreaMessageField.getText().toString() + str);
		textAreaMessageField.positionCaret(textAreaMessageField.getText().toString().length() - 1);
		if(str.contains("WHOISIN")) {
			String[] strList;
			strList = str.split("  ");
			
			ObservableList<String> obList = null;
			obList = FXCollections.observableArrayList("Bus", "Train", "Car");
			
			listViewWhoIsOnline = new ListView<>(obList);
 		}
	}	
}
